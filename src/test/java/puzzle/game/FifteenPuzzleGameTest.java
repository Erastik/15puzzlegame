package puzzle.game;

import org.junit.Test;

import static org.junit.Assert.*;

public class FifteenPuzzleGameTest {

    @Test
    public void newBoard_factoryMethodReturnsNotNull() {

        FifteenPuzzleGame game = FifteenPuzzleGame.newGame();
        assertNotNull(game);
    }

    @Test
    public void newBoard_boardHasSize4x4() {

        FifteenPuzzleGame game = FifteenPuzzleGame.newGame();

        assertEquals(4, game.getBoard().length);
        assertEquals(4, game.getBoard()[0].length);
    }

    @Test
    public void newBoard_validTilesIsMovableHorizontally() {

        FifteenPuzzleGame game = FifteenPuzzleGame.newGame();

        int emptyTileX = -1;
        int emptyTileY = -1;

        int[][] board = game.getBoard();

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {

                if (board[i][j] == 0) {
                    emptyTileX = i;
                    emptyTileY = j;
                }

            }
        }

        assertTrue(emptyTileX > -1);

        int movableTileX = emptyTileX  > 0 ? emptyTileX - 1 : emptyTileX + 1;

        boolean moved = game.moveTile(board[movableTileX][emptyTileY]);

        assertTrue(moved);
        assertFalse(equalsBoards(board, game.getBoard()));
    }

    @Test
    public void newBoard_validTilesIsMovableVertically() {

        FifteenPuzzleGame game = FifteenPuzzleGame.newGame();

        int emptyTileX = -1;
        int emptyTileY = -1;

        int[][] board = game.getBoard();

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {

                if (board[i][j] == 0) {
                    emptyTileX = i;
                    emptyTileY = j;
                }

            }
        }

        assertTrue(emptyTileX > -1);

        int movableTileY = emptyTileY  > 0 ? emptyTileY - 1 : emptyTileY + 1;

        boolean moved = game.moveTile(board[emptyTileX][movableTileY]);

        assertTrue(moved);
        assertFalse(equalsBoards(board, game.getBoard()));
    }

    @Test
    public void newBoard_emptyTileIsNotMovable() {

        FifteenPuzzleGame game = FifteenPuzzleGame.newGame();

        int emptyTileX = -1;
        int emptyTileY = -1;

        int[][] board = game.getBoard();

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {

                if (board[i][j] == 0) {
                    emptyTileX = i;
                    emptyTileY = j;
                }

            }
        }

        assertTrue(emptyTileX > -1);

        boolean moved = game.moveTile(board[emptyTileX][emptyTileY]);

        assertFalse(moved);
        assertTrue(equalsBoards(board, game.getBoard()));
    }

    @Test
    public void newBoard_notValidTileIsNotMovable() {

        FifteenPuzzleGame game = FifteenPuzzleGame.newGame();

        int emptyTileX = -1;
        int emptyTileY = -1;

        int[][] board = game.getBoard();

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {

                if (board[i][j] == 0) {
                    emptyTileX = i;
                    emptyTileY = j;
                }

            }
        }

        assertTrue(emptyTileX > -1);

        int notMovableTileX = emptyTileX  >= 2 ? emptyTileX - 2 : emptyTileX + 2;

        boolean moved = game.moveTile(board[notMovableTileX][emptyTileY]);

        assertFalse(moved);
        assertTrue(equalsBoards(board, game.getBoard()));
    }


    private boolean equalsBoards(int[][] board1, int[][] board2) {

        if (board1.length != board2.length) {
            return false;
        }

        for (int i = 0; i < board1.length; i++) {
            if (board1[i].length != board2[i].length) {
                return false;
            }
            for (int j = 0; j < board1[i].length; j++) {
                if (board1[i][j] != board2[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

}