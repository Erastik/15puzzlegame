package puzzle.game;

public interface NPuzzleGame {

    int[][] getBoard();
    boolean moveTile(int tile);
    boolean isSolved();
}
