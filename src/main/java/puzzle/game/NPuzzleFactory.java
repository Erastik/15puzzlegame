package puzzle.game;

public interface NPuzzleFactory {
    NPuzzleGame newGame();
}
