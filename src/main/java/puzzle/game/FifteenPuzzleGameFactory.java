package puzzle.game;

public class FifteenPuzzleGameFactory implements NPuzzleFactory {

    @Override
    public NPuzzleGame newGame() {
        return FifteenPuzzleGame.newGame();
    }
}
