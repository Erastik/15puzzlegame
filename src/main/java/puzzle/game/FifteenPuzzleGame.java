package puzzle.game;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public final class FifteenPuzzleGame implements NPuzzleGame {

    private static final int[] TILES_PROTOTYPE = new int[] {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0};

    private final int[][] board = new int[4][4];

    private FifteenPuzzleGame() {
    }

    @Override
    public int[][] getBoard() {
        return getBoardCopy(); //Returns copy of board to protect against modifying
    }

    @Override
    public boolean moveTile(int tile) {

        if (tile < 1 || tile > 15) {
            return false;
        }

        return doMove(tile);
    }

    @Override
    public boolean isSolved() {

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] != TILES_PROTOTYPE[i * 4 + j]) {
                    return false;
                }
            }
        }
        return true;
    }

    private void init() {

        int[] tiles = new int[TILES_PROTOTYPE.length];
        do {
            System.arraycopy(TILES_PROTOTYPE, 0, tiles, 0, TILES_PROTOTYPE.length);
            shuffleTiles(tiles);
        } while (!isSolvable(tiles));

        int row = 0;
        for (int i = 0; i < 16; i++) {
            if (i > 0 && i % 4 == 0) {
                row++;
            }
            board[row][i % 4] = tiles[i];
        }
    }

    private void shuffleTiles(int[] tails) {
        Random random = ThreadLocalRandom.current();
        for (int i = tails.length - 1; i > 0; i--) {
            int r = random.nextInt(i + 1);
            int temp = tails[r];
            tails[r] = tails[i];
            tails[i] = temp;
        }
    }

    private boolean isSolvable(int[] tiles) {
        int totalInversions = 0;

        for (int i = 0; i < tiles.length; i++) {
            if (tiles[i] == 0) {
                totalInversions += (i / 4);
            } else {
                for (int j = i + 1; j < tiles.length; j++) {
                    if (j != 0 && tiles[j] > tiles[i]) {
                        totalInversions++;
                    }
                }
            }
        }
        return totalInversions % 2 == 0;
    }

    private int[][] getBoardCopy() {
        int[][] copy = new int[4][];

        for (int i = 0; i < board.length; i++) {
            copy[i] = new int[board[i].length];
            System.arraycopy(board[i], 0 , copy[i], 0, board[i].length);
        }

        return copy;
    }

    private boolean doMove(int tile) {

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == tile) {
                    if (i > 0 && board[i - 1][j] == 0) {
                        swap(i, j, i - 1, j);
                        return true;
                    } else if (i < board.length - 1 && board[i + 1][j] == 0) {
                        swap(i, j, i + 1, j);
                        return true;
                    } else if (j > 0 && board[i][j - 1] == 0) {
                        swap(i, j, i, j - 1);
                        return true;
                    } else if (j < board[i].length - 1 && board[i][j + 1] == 0) {
                        swap(i, j, i, j + 1);
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }

        return false;
    }

    private void swap(int x1, int y1, int x2, int y2) {
        int temp = board[x1][y1];
        board[x1][y1] = board[x2][y2];
        board[x2][y2] = temp;
    }

    public static FifteenPuzzleGame newGame() {
        FifteenPuzzleGame game = new FifteenPuzzleGame();
        game.init();
        return game;
    }
}
