package puzzle;

import puzzle.cli.CliController;
import puzzle.game.FifteenPuzzleGameFactory;

public class PuzzleApplication {

    public static void main(String[] args) {
        new CliController(new FifteenPuzzleGameFactory()).run();
    }
}
