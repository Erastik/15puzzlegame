package puzzle.cli;

import puzzle.game.NPuzzleFactory;
import puzzle.game.NPuzzleGame;

import java.util.Scanner;

public class CliController implements Runnable {

    private static final String EXIT_COMMAND = "exit";
    private static final String NEW_GAME_COMMAND = "new";

    private NPuzzleFactory nPuzzleFactory;
    private Scanner scanner = new Scanner(System.in);

    public CliController(NPuzzleFactory nPuzzleFactory) {
        this.nPuzzleFactory = nPuzzleFactory;
    }

    @Override
    public void run() {

        printGreetings();

        NPuzzleGame game = nPuzzleFactory.newGame();
        int maxCell = (int)Math.pow(game.getBoard().length, 2) - 1;

        boolean exitCommand = false;

        while (!exitCommand) {

            printBoard(game.getBoard());
            printControls();

            String command = nextCommand();

            System.out.println("-------------------------------------");
            if (EXIT_COMMAND.equals(command)) {
                exitCommand = true;
                System.out.println("Exit the game...");
            } else if (NEW_GAME_COMMAND.equals(command)) {
                game = nPuzzleFactory.newGame();
                System.out.println("Starting new game...");
            } else if (command.matches("-?(0|[1-9]\\d*)")) {

                try {
                    int tile = Integer.parseInt(command);
                    processGame(game, tile, maxCell);
                    if (game.isSolved()) {
                        System.out.println("YOU WIN!!!");
                        exitCommand = true;
                    }
                } catch (NumberFormatException e) {
                    printLimits(maxCell);
                }
            } else {
                System.out.println("Unknown command: " + command);
            }

            System.out.println("-------------------------------------");
        }

        System.exit(0);
    }

    private String nextCommand() {
        System.out.println();
        System.out.print("command: ");
        String command = scanner.nextLine();
        System.out.println();
        return command;
    }

    private void processGame(NPuzzleGame game, int tile, int maxCell) {

        if (tile > maxCell || tile < 1) {
            printLimits(maxCell);
        } else {
            boolean result = game.moveTile(tile);
            if (!result) {
                System.out.println("You can move only tiles which have adjacent empty tile");
            } else {
                System.out.println("Cell " + tile + " was moved");
            }
        }
    }
    
    private void printGreetings() {
        System.out.println("------------------------------");
        System.out.println("| Welcome to the Puzzle Game |");
        System.out.println("------------------------------");
        System.out.println();
    }

    private void printBoard(int[][] board) {
        System.out.println();
        System.out.println("Your board:");

        for(int[] row: board) {
            System.out.print("|");
            for (int tile: row) {
                if (tile == 0) {
                    System.out.print("  |");
                } else if (tile < 10) {
                    System.out.print(" " + tile + "|");
                } else {
                    System.out.print(tile + "|");
                }
            }
            System.out.println();
        }
    }

    private void printControls() {
        System.out.println();
        System.out.println("-- enter number of tile (e.g. 2) to move it");
        System.out.println("-- exit - to exit the game.");
        System.out.println("-- new - to start new game.");
    }


    private void printLimits(int limit) {
        System.out.println("Number should be between 1 and " + limit);
    }
}
